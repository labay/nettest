FROM alpine:latest

ADD spinup.sh ./spinup.sh

RUN chmod +x spinup.sh

RUN apk update && apk add openssh sshpass speedtest-cli

CMD ["/spinup.sh"]