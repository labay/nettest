#! /bin/ash

if [ ! -f "spinup" ]; then 
    echo "First run. Creating and sending SSH keys."

    ssh-keygen -f /root/.ssh/id_rsa -q -N ""
    sshpass -p "1rch1ve" ssh-copy-id -p 23 root@192.168.1.132

    touch spinup
    exit 0
else

    while [ 1==1 ]
    do
        echo "Running a speedtest"
        speedtest-cli --csv > $container_name.csv

        echo "Uploading"
        sshpass -p "1rch1ve" scp -o StrictHostKeyChecking=no -P 23 ./$container_name.csv root@192.168.1.132:/Speedtest/$container_name.csv
        sleep 5
    done
fi