#! /bin/ash

START=$1
END=$2
HOST=$3
NETWORK=$4

for (( i=$START; i<=$END; i++ ))
do
    sudo docker run --name speedtest_$i --env container_name=$HOST-$i --net $4 speedtest
done