import pandas as pd
from sqlalchemy import create_engine
import subprocess as sp
import os
from pathlib import Path
from io import StringIO
import argparse

DBLOC = '192.168.1.132'

def SyncTests(files): 
    # Instantiate the DataFrame
    cols = ['serverid', 'sponsor', 'servername', 'testtimestamp', 'distance', 'ping', 'download', 'upload', 'share', 'ipaddress']

    # Import the data files
    T = []
    for f in files:
        t = pd.read_csv(f, header=None).fillna('')
        t.columns = cols
        t['container_name'] = str(f)[:-4]
        T.append(t)
    Tests = pd.concat(T)

    # Push to the DB
    engine = create_engine('postgresql://postgres:EdOps2022!@' + DBLOC + ':5432/postgres')
    conn = engine.connect()
    Tests.to_sql('speedtest', conn, schema='public', index=False, if_exists='append')
    conn.close()

    # Clean up
    for f in files:
        os.remove(f)

def GetFiles():
    ROOTPATH = Path(__file__).parent

    # List the new files
    files = list(ROOTPATH.glob('**/*.csv'))

    return files

if __name__ == "__main__":
    files = GetFiles()
    print('Found %d files'%len(files))
    if len(files) > 0:
        SyncTests(files)